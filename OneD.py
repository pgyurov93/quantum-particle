from Dimension import *


class OneD(Dimension):
    def wavefunction_1d(self, p):  # p and q are quantum numbers, a and b are the dimensions of the box
        X = np.arange(0, 10, 0.05)
        psi = abs((np.sin((p * np.pi * X) / 10))) ** 2
        return psi

    def makePlot(self, frame):
        # Creates canvas
        self.fig = Figure(figsize=(5, 4), dpi=100)  # makes a figure object
        self.figPlot = self.fig.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.fig, frame)
        self.canvas.show()
        self.canvas.get_tk_widget().grid(row=0, column=1)
        # adds a standard plot toolbar
        self.toolbar = NavigationToolbar2TkAgg(self.canvas, frame)
        self.toolbar.update()
        self.toolbar.grid(row=7, column=1)

    def makeInputs(self, frame):
        # builds the frame which will hold all the inputs and their labels
        InputFrame = Frame(frame)
        # places this frame at co-ord (0,0) of the master frame
        InputFrame.grid(column=0, row=0)

        # LABELS FOR QUANTUM NUMBERS
        self.lblp = Label(InputFrame, text="Quantum Number p", justify=LEFT)  # p Label
        self.lblp.grid(column=0, row=0)

        # ENTRY BOXES FOR QUANTUM NUMBERS
        self.entp = Entry(InputFrame)  # p Entry Box
        self.entp.grid(column=1, row=0)

        # Plot Wavefunction Button
        self.butPlot = Button(InputFrame, text='PLOT', command=self.calcPatternWave)
        self.butPlot.grid(column=0, row=9)

        # Clear figure Button
        self.butClear = Button(InputFrame, text='CLEAR GRAPH', command=self.clear)
        self.butClear.grid(column=1, row=9)

    def clear(self):
        self.figPlot.clear()  # clear the axis
        self.canvas.show()  # update the canvas

    def calcPatternWave(self):
        p = int(self.entp.get())  # .get returns a string from that object
        X = np.arange(0, 10, 0.05)
        self.figPlot.plot(X, self.wavefunction_1d(p))
        self.canvas.show()  # update the canvas
