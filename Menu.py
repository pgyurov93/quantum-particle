from OneD import OneD
from TwoD import TwoD
from ThreeD import ThreeD
from Energy import Energy
from Dimension import *


class Menu(Dimension):
    def __init__(self, master):
        frame = Frame(master, cursor="ul_angle", padx=-4) #Frame which will hold everything
        frame.pack()
        self.makeCanvas(frame) #Calls the functions to be used

    def makeCanvas(self, frame):
        #Canvas widget:
        self.canvas = Canvas(frame, width=1000, height=450, bd=-3)
        self.canvas.pack()
        self.image = Image.open("Background.jpg")
        self.photo = ImageTk.PhotoImage(self.image)
        self.canvas.create_image(0,0, image=self.photo, anchor='nw')

        #Create Button 1: 1D POTENTIAL
        self.image2 = Image.open("Button1.jpg")
        self.photo2 = ImageTk.PhotoImage(self.image2)
        self.b = Button(frame, image=self.photo2, compound='right', anchor='w', highlightthickness=0, bd=0, command=self.OpenPage1)
        self.canvas.create_window(800, 100, anchor='nw', window=self.b)

        #Create PDF 1 Button:
        self.imageP1 = Image.open("PDFButton1.jpg")
        self.photoP1 = ImageTk.PhotoImage(self.imageP1)
        self.pdf1 = Button(frame, image=self.photoP1, compound='right', anchor='w', highlightthickness=0, bd=0, command=self.OpenPDF1, relief=FLAT)
        self.canvas.create_window(755, 100, anchor='nw', window=self.pdf1)

        #Create Button 2: 2D POTENTIAL
        self.image3 = Image.open("Button2.jpg")
        self.photo3 = ImageTk.PhotoImage(self.image3)
        self.b2 = Button(frame, image=self.photo3, compound='right', anchor='w', highlightthickness=0, bd=0, command=self.OpenPage2)
        self.canvas.create_window(800, 150, anchor='nw', window=self.b2)

        #Create Button 3: 3D POTENTIAL
        self.image4 = Image.open("Button3.jpg")
        self.photo4 = ImageTk.PhotoImage(self.image4)
        self.b3 = Button(frame, image=self.photo4, compound='right', anchor='w', highlightthickness=0, bd=0, command=self.OpenPage3)
        self.canvas.create_window(800, 200, anchor='nw', window=self.b3)

        #Create Button 4: ENERGY VS QUANTUM NUMBER GRAPH
        self.image5 = Image.open("Button4.jpg")
        self.photo5 = ImageTk.PhotoImage(self.image5)
        self.b4 = Button(frame, image=self.photo5, compound='right', anchor='w', highlightthickness=0, bd=0, command=self.OpenPage4)
        self.canvas.create_window(800, 250, anchor='nw', window=self.b4)
    def OpenPDF1(self):
        os.startfile("PDF1.pdf")
    def OpenPage1(self): #  Page 1 -> 1D Potential
        root = Tk()
        WAVEPLOT1D = OneD(root)
        root.wm_title("1D Potential")
        root.mainloop()
    def OpenPage2(self): # Page 2 -> 2D Potential
        root = Tk()
        WAVEPLOT2D = TwoD(root)
        root.wm_title("2D Potential")
        root.mainloop()
    def OpenPage3(self): # Page 3 -> 3D Potential
        root = Tk()
        WAVEPLOT3D = ThreeD(root)
        root.wm_title("3D Potential")
        root.mainloop()
    def OpenPage4(self): #Page 4 -> Energy vs Quantum Number plot
        root = Tk()
        EnKnplot = Energy(root)
        root.wm_title("Quantisation")
        root.mainloop()
