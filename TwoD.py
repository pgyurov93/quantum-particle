from Dimension import *

def wavefunction2D(p, q, a, b, S):  # p and q are quantum numbers, a and b are the dimensions of the box
    X = np.arange(0,a,S)
    Y = np.arange(0,b,S)
    X, Y = np.meshgrid(X,Y)
    psi = abs((np.sin((p*np.pi*X))*np.sin((q*np.pi*Y))))**2
    return psi


class TwoD(Dimension):

    def makePlot(self, frame):
        # Creates canvas
        self.fig = Figure(figsize=(5,4), dpi=100)  # makes a figure object
        self.figPlot = self.fig.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.fig,frame)
        self.canvas.show()
        self.canvas.get_tk_widget().grid(row = 0, column = 1)
        self.toolbar = NavigationToolbar2TkAgg(self.canvas, frame)  # adds a standard plot toolbar
        self.toolbar.update()
        self.toolbar.grid(row = 7, column = 1)

    def wavefunction_2d(self, p, q, a, b, S):  # p and q are quantum numbers, a and b are the dimensions of the box
        X = np.arange(0,a,S)
        Y = np.arange(0,b,S)
        X, Y = np.meshgrid(X,Y)
        psi = abs((np.sin((p*np.pi*X))*np.sin((q*np.pi*Y))))**2
        return psi

    def makeInputs(self, frame):

        InputFrame = Frame(frame)  # builds the frame which will hold all the inputs and their labels
        InputFrame.grid(column = 0, row = 0)  # places this frame at co-ord (0,0) of the master frame

        # LABELS FOR QUANTUM NUMBERS
        self.lblp = Label(InputFrame, text = "Quantum Number p", justify=LEFT)  # p Label
        self.lblp.grid(column= 0,row= 0)
        self.lblq = Label(InputFrame, text = "Quantum Number q", justify=LEFT) # q Label
        self.lblq.grid(column= 0,row= 1)

        # LABELS FOR DIMENSIONS
        self.lbla = Label(InputFrame, text = "Length a", justify=LEFT)  # a Label
        self.lbla.grid(column= 0,row= 2)
        self.lblb = Label(InputFrame, text = "Length b", justify=LEFT)  # b Label
        self.lblb.grid(column= 0,row= 3)

        # LABELS FOR SMOOTHNESS SLIDER
        self.lblS = Label(InputFrame, text = "Adjust Smoothness", justify=LEFT)  # S Label
        self.lblS.grid(column= 0,row= 4)

        # ENTRY BOXES FOR QUANTUM NUMBERS
        self.entp = Entry(InputFrame)  # p Entry Box
        self.entp.grid(column = 1, row = 0)
        self.entq = Entry(InputFrame)  # q Entry Box
        self.entq.grid(column = 1, row = 1)

        # ENTRY BOXES FOR DIMENSIONS
        self.enta = Entry(InputFrame)  # p Entry Box
        self.enta.grid(column = 1, row = 2)
        self.entb = Entry(InputFrame)  # q Entry Box
        self.entb.grid(column = 1, row = 3)

        # ENTRY BOXES FOR SMOOTHNESS SLIDER
        self.Slider = Scale(InputFrame, from_=0.01, to=0.1, orient=HORIZONTAL, resolution=0.01)  # S Entry Box
        self.Slider.set(0.1)  # set the slider to start at the max value
        self.Slider.grid(column = 1, row = 4)

        # Plot Wavefunction Button
        self.butPlot = Button(InputFrame, text ='PLOT', command = self.calcPatternWave)
        self.butPlot.grid(column = 0, row = 9)

    def calcPatternWave(self):
        p = int(self.entp.get())  # .get returns a string from that object
        q = int(self.entq.get())
        a = int(self.enta.get())  # .get returns a string from that object
        b = int(self.entb.get())
        S = float(self.Slider.get())
        X = np.arange(0,a,S)
        Y = np.arange(0,b,S)
        X, Y = np.meshgrid(X,Y)
        ax = Axes3D(self.fig)
        ax.plot_surface(X, Y, self.wavefunction_2d(p,q,a,b,S), rstride=1, cstride=1, cmap=plt.cm.winter)
        # file = ("Elevator Music.mp3")        #Plays soothing music while you wait for the plot to render
        # clip = mp3play.load(file)
        # clip.play()
        # time.sleep(min(11, clip.seconds()))
        # clip.stop()
        self.canvas.show()  # update the canvas
