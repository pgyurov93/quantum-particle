import mp3play
import time
import os
import pylab
from Tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np
import tkMessageBox
from PIL import Image, ImageTk
import matplotlib.pyplot as plt
from mayavi.mlab import *


class Dimension:
    def __init__(self, master):
        # Create main frame
        frame = Frame(master, cursor="ul_angle")
        frame.pack()
        # Calls the functions to be used
        self.makePlot(frame)
        self.makeInputs(frame)

    def makePlot(self, frame):
        pass

    def makeInputs(self, frame):
        pass

    def clear(self):
        self.figPlot.clear()  # clear the axis
        self.canvas.show()  # update the canvas
